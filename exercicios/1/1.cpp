#include <iostream>

#define NOT 0
#define YES 1

int verifica_palavra(char * palavra, int tam)
{
	int i = 0;
	if (tam > 0) // A palavra deve ter um tamaho
	{
		int lim = tam/2; // Limitar à metade da palavra
		while (i < lim) //enquanto não chegar à metade, verificar
		{
			if (palavra[i] != palavra[tam-i-1]) // Uma diferente, pare
			{
				return NOT;
			}
			i++;
		}
		return YES; //Se chegou à metade, é palindrome
	}
	return NOT;
}

int main(void)
{
	char buffer;
	char entrada[40];
	int n, i = 0, j;

	std::cin >> n; //Número de linhas a serem lidas
	std::cin.ignore(1, '\n');
	for (i = 0; i < n; i++) //Pegar as n palavras
	{
		j = 0; //Reiniciar contador apís cada palavra
		do
		{			
			std::cin.get(buffer); //Forçar a ler uma letra por vez
			if(buffer != ' ' && buffer != '\n') //Ignorar os espaços
			{
				entrada[j++] = buffer; //Escrever tudo como uma palavra só
			}
		} while (buffer != '\n');
		if (verifica_palavra(entrada, j)) //Se a palavra for palindroma, imprima yes
		{
			std::cout << "yes" << '\n';
		}
		else
		{
			std::cout << "not" << '\n';
		}
	}
}
