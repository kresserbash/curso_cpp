#include <iostream>
#include <string.h>

#define WIN 1
#define LOSE 0

class Carta
{
	public:
		int qtd;
		void get_nome(char * cpy);
		void get_desc(char * cpy);
		int get_qtd(void);

	private:
		char nome[30];
		char desc[200];
};

void Carta::get_nome(char * cpy)
{
	strcpy(cpy, nome);
}

void Carta::get_desc(char * cpy)
{
	strcpy(cpy, desc);
}

int Carta::get_qtd(void)
{
	return qtd;
}

class Equipamento : public Carta
{
	public:
		char typ;
		int buf;
		Equipamento(char * nome, char * desc, int qtd, char typ, int buf);
};

Equipamento::Equipamento(char * nome, char * desc, int qtd, char typ, int buf)
{
	Equipamento::typ = typ;
	Equipamento::buf = buf;
	Equipamento::qtd = qtd;
	strcpy(Equipamento::nome, nome);
	strcpy(Equipamento::desc, desc);
}

class Monstro : public Carta
{
	public:
		//Atributos
		int atk, def, eqp;
		class Equipamento * equip[5];
		//Métodos
		Monstro(char * nome, char * desc, int qtd, int atk, int def);
		int get_atk(void){ return atk; };
		int get_def(void){ return def; };
		int fight(class Monstro monstro);
		void equipar(class Equipamento * equip);
	private:
		void set_atk(int atk);
		void set_def(int def);
};

Monstro::Monstro(char * nome, char * desc, int qtd, int atk, int def)
{
	Monstro::atk = atk;
	Monstro::def = def;
	Monstro::eqp = 0;
	Monstro::qtd = qtd;
	strcpy(Monstro::nome, nome);
	strcpy(Monstro::desc, desc);
}

int Monstro::fight(class Monstro monstro)
{
	if (Monstro::get_atk() > monstro.get_def())
	{
		return WIN;
	}
	return LOSE;
}

void Monstro::equipar(class Equipamento * equip)
{
	if(Monstro::eqp < 5 && equip != NULL)
	{
		Monstro::equip[Monstro::eqp] = equip;
	}
}

int main (void)
{
	int max = 30;
	Monstro baralho_monster[max];
	Equipamento baralho_equip[max];
}

