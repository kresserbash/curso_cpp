# 3 - Orientação a Objetos

## 3.0 - Exemplos de programa

[Programa 0000](aulas/3/00.cpp)

[Programa 0001](aulas/3/01.cpp)

[Programa 0011](aulas/3/02.cpp)

[Programa 0010](aulas/3/03.cpp)

[Programa 0110](aulas/3/04.cpp)

[Programa 0111](aulas/3/05.cpp)

[Programa 0101](aulas/3/06.cpp)

[Programa 0100](aulas/3/07.cpp)

[Programa 1100](aulas/3/08.cpp)

[Programa 1101](aulas/3/09.cpp)

[Programa 1111](aulas/3/10.cpp)

[Programa 1110](aulas/3/11.cpp)

Compile e rode.

```
[allan@home ~]$ g++ 1.cpp -o 1
[allan@home ~]$ ./1
```

## 3.1 - Definição e utilidade 

Primeiro, precisamos mudar a forma de interpretação dos programas. Não
precisamos mais pensar em um programa contínuo, e sim algo mais atômico,
divisível, independente, enfim. O programa deixa de ser um bloco único e
viram várias peças que se encaixam.

A Orientação a objetos surge da ideia de abstrair objetos do mundo real.
Não que isso não seja possível com programas estruturados, mas se torna
mais simples.

Como um exemplo, imagine um jogo de cartas, onde comparamos os valores
dessas cartas para decidir, baseado em uma série de atributos, qual carta é
mais forte. Vamos modelar assim:

Chamaremos a entidade principal de carta: ela deve possuir um nome, e
atributos específicos como ataque, defesa, e quantidade de cópias que você tem.

```
Carta: 
	Nome: Carta exemplo
	Atributos:
		Ataque: 0
		Defesa: 0
	Quantidade: 1
```

Bacana, determinado brevemente o escopo, parece que podemos começar a
implementar. Em C poderíamos criar uma struct, e atriuir os respectivos
dados à estrutura criada, ou mesmo simplificar ainda mais e usar variáveis
estáticas para tal

[Programa 0000](aulas/3/00.c)

O programa funciona, bacana. Mas vamos deixar um pouco mais complexo. E se
quisermos botar duas cartas para batalhar? precisamos criar uma função para
isso, certo? Basta comparar o ataque com a respectiva defesa, e se o ataque
for maior, a batalha foi decidida.

Mas vamos pensar mais um pouco? E se criassemos cartas, por exemplo, de
equipamento? equipamentos tem um ataque? tem uma defesa? Se o equipamento
fosse uma espada, por exemplo, poderia ter, mas e um colar?

Precisaríamos criar outro tipo de carta para dar conta do recado, e logo já
estamos com duas structs diferentes para administrar. E as funções que
aplicamos no primeiro tipo de carta não serão as mesmas do segundo tipo de
cartas.

O que estou tentando explicar é que daqui alguns ciclos de trabalho, nos
veríamos organizando arquivos gigantescos de bibliotecas, com várias
structs diferentes, cada qual com suas bibliotecas, e sem querer, nosso
programa de C começa a ficar segmentado demais para fazermos uma manutenção
eficiente.

Para isso serve a programação orientada a objetos: Se criamos CLASSES para
determinar quais tipos de cartas temos, podemos estabelecer parâmetros
específicos e arbitrários para cada carta, cada qual com suas funções, e
tudo num programa bem mais simplificado. Vamos ver o mesmo exemplo em C++

[Programa 0000](aulas/3/00.cpp)

Graças a conceitos que veremos mais adiante, esse modelo serviria para todo
e qualquer tipo de cartas no nosso baralho. Conseguimos adaptar as classes
a novos tipos específicos sem especificar outras classes do zero. Você vai
entender o que eu estou falando. Mas primeiro precisamos passar uns
nomes...

	

## 3.2 - Conceitos do paradigma

Primeira coisa. a gente usa a definição objeto da mesma forma como num
mundo real: ele possui atributos específicos (tamanho, cor, marca,
enfim...), e ele pertence à uma classe.

Para facilitar o raciocínio, pense num carro. O carro é uma classe de
automotor: existem vários tipos de automotores que executam a mesma tarefa
de formas diferentes: você pode dirigir moto, ou carro, mas ambos são
dirigidos de formas diferentes.

A uma ocorrência específica de classe damos o nome de objeto: para um carro
específico, nosso objeto pode se chamar Uno, por exemplo.

E esse objeto, novamente, possui atributos: o uno tem uma cor, um tamanho
de roda, a potência do motor (determinada pelo tamanho da escada que ele
carrega), e aceleração maxima, por exemplo (determinado pela logomarca que
ele levar na porta).

Os atributos são, a grosso modo, as variáveis da nossas structs de C. Mas
são, na POO, as características que levam nossos objetos.

Outro conceito importante é o de método. Você pode, literalmente,
interpretar como sendo 'a maneira com que um objeto realiza dada tarefa'.
Retornando ao exemplo dos veículos, o carro e a moto possuem um método
diferente de aceleração, mas podem ter os mesmos atributos (dependendo do
nível de detalhamento do sistema). O método nada mais é que uma função que
o objeto executa, e possui, internamente, para atender as necessidades do
usuário. Atacar, no caso do baralho, acelerar no caso do carro...

Ainda existe o conceito de encapsulamento: por questões de boas praticas de
programação, todas as funções devem funcionar de forma atômica, isto é, ser
um sistema praticamente fechado: recebe uma entrada, e gera uma saída, sem
você precisar saber como ele executa internamente, ou se ele pega dados de
algum outro lugar. Por isso definimos novas palavras chaves: escopo local,
pai, e global, além do rótulo privado, e público. Não se preocupe. Vais
entender melhor. O importante é entender que nem sempre você precisa
construir as coisas do zero.

Mas nem sempre dois objetos de uma mesma classe fazem uso das mesmas
característicasm, o que na programação estruturada nos obrigaria a recriar
a estrutura. Por exemplo a identidade de uma entidade estatal: podemos ter
pessoas físicas, como eu ou você, ou pessoas jurídicas, como as empresas.
Ambos, na lei, se chama "pessoa", mas um é do tipo jurídico, e um do tipo
físico. Ambos possuem nome, e um endereço, mas um possui CPF e o outro
CNPJ. Surge aqui o conceito de HERANÇA.

A partir da herança, podemos criar uma classe apenas com os atributos
comuns de segunda classe, e criar uma chamada "classe de n-ésimo nível" em
que, se uma ocorrência for de pessoa física, apenas adicionamos atributos
de CPF. E não será necessário nenhum outro apontamento para inferir os
dados.





[Programa 0111](aulas/3/05.cpp)

[Programa 0101](aulas/3/06.cpp)

[Programa 0100](aulas/3/07.cpp)

[Programa 1100](aulas/3/08.cpp)

[Programa 1101](aulas/3/09.cpp)

[Programa 1111](aulas/3/10.cpp)

[Programa 1110](aulas/3/11.cpp)



## 3.3 - Exemplo de classe

Retornando ao nossso programa, podemos começar a pensar em algumas coisas
mais complicadas. Vamos redefinir como nossa classe de cartas funcionam.
TODA carta deve ter um nome, certo? e uma quantidade! E também uma
descrição. Isso é presente à
toda carta, independente de seu tipo. Essa será nossa classe básica.

[Programa 0001](aulas/3/01.cpp)

Perceba que na chamada de classe, não precisamos fazer uso da palavra
reservada 'class'. Ela é arbitrária. Mas seria necessário, por exemplo, se
tanto uma classe como uma variável tivessem o mesmo nome.

## 3.4 - Encapsulamento

O encapsulamento diz respeito a fechar as janelas de uma classe para o
usuário saber como ela roda por dentro. As palavras reservadas de protected
e private servem para isso: definir quem consegue acessar, ou não, os
atributos e métodos de uma função. Você pode, por exemplo, receber um dado
qualquer, e tentar atribuir à uma variável publica da sua classe. Se ela
for pública, tudo certo. Mas pode não ser...

[Programa 0011](aulas/3/02.cpp)

O mesmo acontece para métodos, e sobreposição, por exemplo.

Existe ainda o rótulo protected. Enquato TUDO pode aceesar o que for
publico de um objeto, e NADA pode acessar o que for privado de um objeto
(a não ser o próprio objeto), o protected diz que se a classe derivar algo,
como outra classe, ou um objeto, enfim, eles poderão acessar. Mas uma
classe distinta, por exemplo, não poderia acessar.

### Para Considerar

Como definimos o que deixar privado, publico, ou protegido?

Para resolver esses problemas de acesso, parametrizamos todos os acessos
usando um padrão de "busca", e outro de "define", que chamamos de "setter",
"construtora" ou "manipuladora", e "getter", "função de interface" ou
"extratora".

## 3.5 - Construtores e destrutores

Os métodos construtores servem para configurar os valores iniciais da
classe, e geralmente dar-se-à nomes parametrizados para que TODA classe
possua um padrão de pré-definição desses parâmetros. Assim, os construtores
terão um nome determinado independente da classe.

[Programa 0010](aulas/3/03.cpp)

Como você pode ver, a função destrutora também foi inserida. A função
destrutora não possui outra função se não destruir e liberar os dados de
objeto que você criou. A única diferença declarativa é o til que antecede o
nome.

## 3.6 - Herança

Por fim, a herança. A herança surge da ideia, que dados dois objetos
semelhantes, você precisa determinar certos atributos que o outro não tem
em comum, e em vez de redeclarar a estrutura, e definir ambos como sendo
exatamente a mesma coisa, você apenas define algums atributos novos, e
especifica ambos como fazendo parte de uma organização comum, e maior.

[Programa 0110](aulas/3/04.cpp)

