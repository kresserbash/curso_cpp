#include <iostream>
#include <iomanip>

int main(void)
{
	int lim = 0, val;

	std::cin >> lim;
	for (int i = 1; i <= lim; i++)
	{
		std::cin >> val;
		std::cout << std::setw(i);
		std::cout << val << '\n';
	} 
}
